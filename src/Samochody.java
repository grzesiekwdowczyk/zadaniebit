public class Samochody   {
    public String markaSamochodu;
    public String typSilnika;
    public int prędkość;

    Samochody(String markaSamochodu, String typSilnika, int prędkość){
        this.markaSamochodu = markaSamochodu;
        this.typSilnika = typSilnika;
        this.prędkość = prędkość;
    }

    public void informacjeOsamochodzie (){
        System.out.println(this.markaSamochodu + " " + this.typSilnika + " " + this.prędkość);
    }

}